import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloflutterAppstate createState() => _HelloflutterAppstate();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Halo Flutter";
String japanGreeting = "Konnichiwa Flutter";

class _HelloflutterAppstate extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = englishGreeting;
                    });
                  },
                  icon: Icon(Icons.refresh)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText =  spanishGreeting;
                    });
                  },
                  icon: Icon(Icons.favorite)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = japanGreeting;
                    });
                  },
                  icon: Icon(Icons.grade)),
            ],
          ),
          body: Center(
            child: Text(
              displayText,
              style: TextStyle(fontSize: 24),
            ),
          )),
    );
  }
}

/*class HelloFlutterApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home : Scaffold(
        appBar: AppBar(
            title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: (){
                  setState((){

                  });
                icon: Icon(Icons.refresh))
          ],
        ),

          body: Center(
            child: Text(
                "Hello Flutter",
            style: TextStyle(fontSize: 24),),
          )
      ),
    );
  }
}*/
